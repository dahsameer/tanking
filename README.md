# tanking

## What is this?
This is a mini game made in java. It was to learn the basics of Java
This project was made in eclipse so it can easily be imported in eclipse as well as netbeans.
I used openjdk-10 so some code might or might not generate error if you're using different version

## How to use?
Window.java has the main class. To build the project, you can perform
```bash
javac Window.java
```

and to run it, you must perform
```bash
java Window
```

## How to play
* Run the game
* Click Start
* W/S/A/D/F - increase fire power / decrease fire power / move left / move right / Fire [for tank 1]
* Up/Down/Left/Right/Space - increase fire power / decrease fire power / move left / move right / Fire [for tank 2]

## Issues
* After you click start game, the Game panel doesn't get focus so you show lose the focus from the Window and again gain it. Click elsewhere than the game window for it
* The game detects Game over but nothing happens. I have yet to implement it
* Tanks can still fire after dying
