import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Window extends JFrame{
	
	public JPanel menu;
	public Game game;
	public GameOver gameo;

Window() {
	menu = new Menu(this);
	gameo = new GameOver(this);
	this.getContentPane().add(menu);
	game = new Game(this);
	game.addKeyListener(game);
    setResizable(false);
    pack();

    setTitle("Tanking");
    setLocationRelativeTo(null);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
}


public static void main(String[] args) {

    EventQueue.invokeLater(new Runnable() {
        @Override
        public void run() {
            JFrame frame = new Window();
            frame.setVisible(true);
        }
    });
}
}