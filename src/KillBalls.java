import java.awt.Rectangle;

public class KillBalls implements Runnable{
	
	private Tank tank1, tank2;
	Thread t;
	Window mainFrame;
	public KillBalls(Tank t1, Tank t2,  Window m) {
		tank1 = t1;
		tank2 = t2;
		mainFrame = m;
	}

	@Override
	public void run() {
		while(true && t!=null) {
			for(int i=0; i<tank1.bombs.size(); i++) {
				if(new Rectangle(tank2.getCornerPositionX(),tank2.getCornerPositionY(),75,50).intersects(new Rectangle(tank1.bombs.get(i).getPositionX(),tank1.bombs.get(i).getPositionY(),10,10))) {
					tank1.bombs.remove(i);
					tank2.decreaseLives();
				}
				else if(tank1.bombs.get(i).getPositionY()>=505) {
					tank1.bombs.remove(i);
				}
			}
			for(int i=0; i<tank2.bombs.size(); i++) {
				if(new Rectangle(tank1.getCornerPositionX(),tank1.getCornerPositionY(),75,50).intersects(new Rectangle(tank2.bombs.get(i).getPositionX(),tank2.bombs.get(i).getPositionY(),10,10))) {
					tank2.bombs.remove(i);
					tank1.decreaseLives();
				}
				else if(tank2.bombs.get(i).getPositionY()>=505) {
					tank2.bombs.remove(i);
				}
			}
			if(tank1.lives==0 || tank2.lives==0) {
				
				System.out.println("DEAD");
				if(tank1.lives==0) {
					mainFrame.gameo.setWinner("Tank 2");
				}
				else {
					mainFrame.gameo.setWinner("Tank 1");
				}
				tank1 = null;
				tank2 = null;
				mainFrame.getContentPane().removeAll();
				mainFrame.game=null;
				mainFrame.getContentPane().add(mainFrame.gameo);
				mainFrame.validate();
				t = null;
			}
			try {
				Thread.sleep(2);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void startThread() {
		if(t==null) {
			t = new Thread(this);
		}
		t.start();
	}

}
