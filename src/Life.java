import java.awt.Image;

import javax.swing.ImageIcon;

public class Life{
	Image Lives0, Lives1, Lives2, Lives3;
	
	Life(){
		Lives0 = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/Lives_0.png").getImage();
		Lives1 = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/Lives_1.png").getImage();
		Lives2 = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/Lives_2.png").getImage();
		Lives3 = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/Lives_3.png").getImage();
	}
	
	public Image getLife(int l) {
		Image toReturn = null;
		if(l==0) {
			toReturn = Lives0;
		}
		else if(l==1) {
			toReturn = Lives1;
		}
		else if(l==2) {
			toReturn = Lives2;
		}
		else if(l==3) {
			toReturn = Lives3;
		}
		return toReturn;
	}
}