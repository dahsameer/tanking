import java.awt.Image;

import javax.swing.ImageIcon;

public class Power {
	
	private Image power0,power1,power2,power3,power4,power5,power6,power7,power8,power9;
	
	public Power() {
		power0 = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/Power0.png").getImage();
		power1 = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/Power1.png").getImage();
		power2 = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/Power2.png").getImage();
		power3 = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/Power3.png").getImage();
		power4 = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/Power4.png").getImage();
		power5 = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/Power5.png").getImage();
		power6 = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/Power6.png").getImage();
		power7 = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/Power7.png").getImage();
		power8 = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/Power8.png").getImage();
		power9 = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/Power9.png").getImage();
	}
	
	public Image getPower(int p) {
		Image toReturn = null;
		switch (p){
		case 400:
			toReturn = power0;
			break;
		case 555:
			toReturn = power1;
			break;
		case 710:
			toReturn = power2;
			break;
		case 865:
			toReturn = power3;
			break;
		case 1020:
			toReturn = power4;
			break;
		case 1175:
			toReturn = power5;
			break;
		case 1330:
			toReturn = power6;
			break;
		case 1485:
			toReturn = power7;
			break;
		case 1640:
			toReturn = power8;
			break;
		case 1795:
			toReturn = power9;
			break;
		}
		return toReturn;
	}
	
}
