public class Bomb implements Runnable{

	private double positionX, positionY, speedX, speedY, speed, deltaTime, gravity;
	private int angle;
	Thread t;
	Tank opponent;
	
	Bomb(int x, int y, int s, Direction d, Tank t) {
		opponent = t;
		speed = s;
		angle = -15;
		
		if(d == Direction.RIGHT) {
			speedX = speed * Math.cos(Math.toRadians(angle));
		}
		else if(d == Direction.LEFT) {
			speedX = -(speed * Math.cos(Math.toRadians(angle)));
		}
		speedY = speed * Math.sin(Math.toRadians(angle));
		positionX = x;
		positionY = y;
		deltaTime = 0.001;
		gravity = 2;
	}
	
	public void updateLocation(){
        positionX += this.speedX * this.deltaTime;
        positionY += this.speedY * this.deltaTime;
        this.speedY += this.gravity;
    }
	
	public int getPositionX() {
		return (int)positionX;
	}
	public int getPositionY() {
		return (int)positionY;
	}

	@Override
	public void run() {
		while(true) {
			updateLocation();
			try {
				Thread.sleep(4);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*
			Rectangle r1 = new Rectangle((int)positionX,(int)positionY,10,10);
			Rectangle r2 = new Rectangle(opponent.getCornerPositionX(),opponent.getCornerPositionY(),75,50);
			if(r1.intersects(r2)) {
				opponent.decreaseLives();
			}*/
		}
		
	}
	
	public void StartThread() {
		if(t==null) {
			t = new Thread(this);
		}
		t.start();
	}
	

}
