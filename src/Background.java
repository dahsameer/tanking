import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Background{
	
	private String filename = "/home/dahsameer/Codes/Java/tanking/src/sounds/background.wav";
	AudioInputStream audioInputStream;
	public Background() {
		try {
			audioInputStream = AudioSystem.getAudioInputStream(new File(filename));
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
			clip.loop(Clip.LOOP_CONTINUOUSLY);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}