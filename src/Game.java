import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class Game extends JPanel implements ActionListener, KeyListener{
	private Timer timer;
	Sound s1,s2;
	Image background;
	ImageIcon temp;
	Tank tank1, tank2;
	ImageIcon tempBomb = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/Bomb.png");
	Image MyBomb;
	Window mainFrame;
	public KillBalls kb;

	
	Game(Window frame){
		mainFrame = frame;
		
	}
	public void gameInit() {
		setPreferredSize(new Dimension(1000, 600));
		requestFocus(true);
		temp = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/background2.png");
		setFocusable(true);
		background = temp.getImage();
		
		setLayout(null);
		tank1 = new Tank(Direction.RIGHT);
		tank2 = new Tank(Direction.LEFT);
		tank1.addOpponent(tank2);
		tank2.addOpponent(tank1);
		kb = new KillBalls(tank1,tank2,mainFrame);
		MyBomb = tempBomb.getImage();
		kb.startThread();
	
		JButton btnHome = new JButton("HOME");
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mainFrame.getContentPane().removeAll();
				mainFrame.getContentPane().add(mainFrame.menu);
				mainFrame.validate();
			}
		});
		btnHome.setBackground(Color.DARK_GRAY);
		btnHome.setForeground(Color.PINK);
		btnHome.setFont(new Font("Agency FB", Font.PLAIN, 24));
		btnHome.setBounds(23, 11, 125, 35);
		add(btnHome);
		
		JButton btnPause = new JButton("PAUSE");
		btnPause.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnPause.setForeground(Color.PINK);
		btnPause.setFont(new Font("Agency FB", Font.PLAIN, 24));
		btnPause.setBackground(Color.DARK_GRAY);
		btnPause.setBounds(854, 11, 125, 35);
		add(btnPause);
		timer = new Timer(17, this);
	    timer.start();
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		draw(g);
	}
	
	public void draw(Graphics g) {
		g.drawImage(background, 0, 0, null);
		g.drawImage(tank1.getTank(), tank1.getCornerPositionX(), tank1.getCornerPositionY(), null);
		g.drawImage(tank1.GetTankLives(), 50,90, null);
		g.drawImage(tank2.GetTankLives(), 840,90,null);
		g.drawImage(tank1.getPower(), tank1.getPositionX()-50,tank1.getPositionY()-50, null);
		g.drawImage(tank2.getPower(), tank2.getPositionX()-50,tank2.getPositionY()-50, null);
		g.drawImage(tank2.getTank(), tank2.getCornerPositionX(), tank2.getCornerPositionY(), null);
		for(int i=0; i<tank1.bombs.size(); i++) {
			g.drawImage(MyBomb, tank1.bombs.get(i).getPositionX(), tank1.bombs.get(i).getPositionY(), null);
		}
		for(int i=0; i<tank2.bombs.size(); i++) {
			g.drawImage(MyBomb, tank2.bombs.get(i).getPositionX(), tank2.bombs.get(i).getPositionY(), null);
		}
		Toolkit.getDefaultToolkit().sync();
	}
		@Override
	public void actionPerformed(ActionEvent e) {
		repaint();
		tank1.move();
		tank2.move();
	}

	@Override
	public void keyPressed(KeyEvent k) {
		//System.out.println("Fuck aba ta chalxa");
		int key = k.getKeyCode();
		switch(key) {
		case KeyEvent.VK_A:
			tank1.startMovingLeft();
			break;
		case KeyEvent.VK_D:
			tank1.startMovingRight();
			break;
		case KeyEvent.VK_W:
			tank1.increasePower();
			break;
		case KeyEvent.VK_S:
			tank1.decreasePower();
			break;
		case KeyEvent.VK_LEFT:
			tank2.startMovingLeft();
			break;
		case KeyEvent.VK_RIGHT:
			tank2.startMovingRight();
			break;
		case KeyEvent.VK_UP:
			tank2.increasePower();
			break;
		case KeyEvent.VK_DOWN:
			tank2.decreasePower();
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent k) {
		int key = k.getKeyCode();
		switch(key) {
		case KeyEvent.VK_A:
		case KeyEvent.VK_D:
			tank1.stopMoving();
			break;
		case KeyEvent.VK_LEFT:
		case KeyEvent.VK_RIGHT:
			tank2.stopMoving();
			break;
		case KeyEvent.VK_F:
			s1 = null;
			s1 = new Sound();
			tank1.fire();
			break;
		case KeyEvent.VK_SPACE:
			s2 = null;
			s2 = new Sound();
			tank2.fire();
			break;
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
	}
}
