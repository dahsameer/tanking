import java.awt.Image;
import java.util.ArrayList;
import javax.swing.ImageIcon;

public class Tank implements Runnable{
	private int positionX;
	private int positionY;
	private Image MyTank;
	private Image normal;
	private Image fired; 
	public int lives;
	private int Power;
	private Power p = new Power();
	private int dbpos;
	private ImageIcon normalIcon, firedIcon;
	public ArrayList<Bomb> bombs;
	Direction direction;
	private boolean MoveLeft;
	private boolean MoveRight;
	private Thread t;
	private Tank opponent;
	Life l = new Life();
	
	Tank(Direction dir){
		lives = 3;
		Power = 400;
		MoveLeft = false;
		MoveRight = false;
		direction = dir;
		if(direction==Direction.RIGHT) {
			dbpos = 30;
			positionX = 100;
			normalIcon = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/TankRightNormal.png");
			firedIcon = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/TankRightFire.png");
		}
		else if(direction==Direction.LEFT) {
			dbpos = -30;
			positionX = 900;
			normalIcon = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/TankLeftNormal.png");
			firedIcon = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/TankLeftFire.png");
		}
		normal = normalIcon.getImage();
		fired = firedIcon.getImage();
		positionY = 500;
		MyTank = normal;
		bombs = new ArrayList<Bomb>();
	}
	
	public Image getTank() {
		return MyTank;
	}
	
	public int getPositionX() {
		return positionX;
	}
	
	public int getPositionY() {
		return positionY;
	}
	
	public int getCornerPositionX() {
		return positionX-37;
	}
	
	public int getCornerPositionY() {
		return positionY-25;
	}
	
	public int getBarrelPositionX() {
		return positionX+dbpos;
	}
	
	public int getBarrelPositionY() {
		return positionY-22;
	}
	
	public void fire() {
		this.startThread();
		bombs.add(new Bomb(getBarrelPositionX(), getBarrelPositionY(), Power, direction, opponent));
		//Power = 800;//reset gareko power lai
		bombs.get(bombs.size()-1).StartThread();
	}
	
	public void move() {
		if(MoveLeft) {
			positionX -= 1;
		}
		else if(MoveRight) {
			positionX += 1;
		}
	}
	
	public void increasePower() {
		Power+=155;
		if(Power>1795) {
			Power = 1795;
		}
	}
	
	public void decreasePower() {
		Power-=155;
		if(Power<400) {
			Power = 400;
		}
	}
	
	public void startMovingLeft() {
		MoveLeft = true;
	}
	public void stopMoving() {
		MoveLeft = false;
		MoveRight = false;
	}
	public void startMovingRight() {
		MoveRight = true;
	}

	@Override
	public void run() {
		MyTank = fired;
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		MyTank = normal;
	}
	
	public void startThread() {
		t=new Thread(this);
		t.start();
	}
	
	public Image GetTankLives() {
		return l.getLife(lives);
	}
	
	public void decreaseLives() {
		lives--;
	}
	
	public Image getPower() {
		return p.getPower(Power);
	}
	
	public void addOpponent(Tank t) {
		opponent = t;
	}
}
