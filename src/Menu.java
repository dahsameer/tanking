import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.UIManager;

@SuppressWarnings("serial")
public class Menu extends JPanel {
	
	Window mainFrame;
	ImageIcon m = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/menu.png");
	Image menu = m.getImage();
	
	/**
	 * Create the panel.
	 */
	public Menu(Window frame) {
		mainFrame = frame;
		setPreferredSize(new Dimension(1000,600));
		setLayout(null);
		
		JButton btnStartGame = new JButton("Start Game");
		btnStartGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mainFrame.getContentPane().removeAll();
				mainFrame.getContentPane().add(mainFrame.game);
				mainFrame.game.gameInit();
				new Background();
				mainFrame.validate();
			}
		});
		btnStartGame.setForeground(UIManager.getColor("controlText"));
		btnStartGame.setBackground(Color.GREEN);
		btnStartGame.setFont(new Font("Algerian", Font.BOLD | Font.ITALIC, 40));
		btnStartGame.setBounds(331, 175, 348, 83);
		add(btnStartGame);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnExit.setForeground(UIManager.getColor("controlText"));
		btnExit.setBackground(Color.RED);
		btnExit.setFont(new Font("Algerian", Font.BOLD | Font.ITALIC, 40));
		btnExit.setBounds(331, 314, 350, 83);
		add(btnExit);

	}
	
	public void paintComponent (Graphics g) {
		super.paintComponent(g);
		draw(g);
	}
	
	public void draw(Graphics g) {
		g.drawImage(menu, 0, 0, null);
		g.setColor(Color.getHSBColor(200, 100, 40));
		g.setFont(new Font("Chiller", Font.BOLD,50));
		g.drawString("Tanking", 450, 50);
		g.setFont(new Font("Lucida Console", Font.PLAIN, 20));
		g.setColor(Color.WHITE);
		g.drawString("A game by Sameer Dahal", 600, 100);
	}
}
