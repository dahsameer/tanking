import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class GameOver extends JPanel implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Image background;
	ImageIcon temp;
	Window mainFrame;
	private Timer timer;
	String winner;
	
	public GameOver(Window w) {
		setPreferredSize(new Dimension(1000, 600));
		setVisible(true);
		temp = new ImageIcon("/home/dahsameer/Codes/Java/tanking/src/img/menu.png");
		background = temp.getImage();
		setFocusable(true);
		mainFrame = w;
		timer = new Timer(1000, this);
	    timer.start();
	}
	
	public void setWinner(String x) {
		winner = x;
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		draw(g);
	}

	
	public void draw(Graphics g) {
		g.drawImage(background, 0, 0, null);
		g.setColor(Color.getHSBColor(200, 100, 40));
		g.setFont(new Font("Chiller", Font.BOLD,50));
		g.drawString("Game Over", 350, 300);
		g.drawString(winner + " Won the game", 250, 500);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		repaint();	
		
	}
}
